package com.demo.camera;


import org.junit.jupiter.api.Assertions;

import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;

public class PhotoCameraTest {

  /*  @Test
    public void changeMe() {
        Assertions
            .assertThat(Arrays.asList(2, 3))
            .isNotNull()
            .contains(3)
            .doesNotContain(5)

        ;

        Assertions
            .assertThat(this)
            .isNotNull();

    }
*/
    @Test
    public void turnOnPhotoCameraShouldTurnOnPowerOfSensor(){

        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.turnOn();

        Mockito.verify(sensor).turnOn();
    }

    @Test
    public void turnOffPhotoCameraShouldTurnOffPowerOfSensor(){

        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.turnOff();

        Mockito.verify(sensor).turnOff();
    }

    @Test
    public void pressingTheShutterIfThePowerIsOffDoesNothing(){
        ImageSensor sensor = mock(ImageSensor.class);
        PhotoCamera camera = new PhotoCamera(sensor);

        camera.turnOff();

        camera.pressButton();

        Mockito.verifyZeroInteractions(sensor);
    }





}